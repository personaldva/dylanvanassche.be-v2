---
layout: post
title:  ESWC2022 Trip Report
date:   2022-06-08 09:00:00
author:
    id: https://dylanvanassche.be/#me
    name: Dylan Van Assche
description: My first in-person conference!
---

[European Semantic Web Conference](https://2022.eswc-conferences.org/)
took place this year (2022) in Crete, Greece
with a lot of sun, awesome people & talks, but how was it there exactly?
I had a pretty booked schedule, I presented in various places of the conference
from workshops to posters. If you attended, I hope you enjoyed my talks and 
if you're interested in my work, feel free to contact me to collaborate 
or discuss ([contact details](https://dylanvanassche.be/))!
Here's my report about the ESWC experience as my first in-person conference from my point of view:

### Day 0: Travelling & schedule

Due to the pandemic, it was ages ago I travelled, especially by plane.
Weeks before the conference, I prepared all my talks, arranged a place to stay,
take care of the flights, etc. 
Took a while to get everything in-place,
but I think it will go faster next time.
I will be travelling soon again in a few weeks, 
anyone who already knows where? 
Let me know on [Twitter](https://twitter.com/DylanVanAssche)!
Anyway, I hopped on to the plane without much issues
and arrived in Crete without much delay in the late evening.
Shared a taxi with some colleagues to the hotel.
However, the receptionist couldn't find our reservation for the rooms.
After some short panic it seems it was registered under a different name,
so luckily, in the end everything was resolved, I had a place to sleep for the night!

ESWC 2022 consisted of 2 days Tutorials & Workshops and
3 days of main conference. Let's go over them!

### Day 1: Tutorials & Workshops I: KGC tutorial by COST-Action

The first day, I attended the
[Knowledge Graph Construction Tutorial](https://kg-construct.github.io/eswc-dkg-tutorial-2022/)
since I was also a presenter there.
This amazing tutorial was organized by [DKG Cost Action](https://cost-dkg.eu/).
We had a full house for this tutorial and even had to move to a bigger room
to fit everyone in! A lot of tools and approaches where presented such as
[Mapeathor](https://github.com/oeg-upm/Mapeathor),
[RocketRML](https://semantifyit.github.io/RocketRML/),
[SDM-RDFizer](https://github.com/SDM-TIB/SDM-RDFizer),
[Dragoman](https://github.com/SDM-TIB/Dragoman),
etc.
It was a great overview of all the different approaches and tools used in
knowledge graph construction, including some hands-on experience!

I presented 2 tools during the tutorial: [YARRRML](https://rml.io/yarrrml)
in the morning and [RMLStreamer](https://github.com/RMLio/RMLStreamer) in the afternoon.
YARRRML is a human-friendly representation of [RML](https://rml.io/spec)
which allows you to write a few YAML-based mappings to transform all kinds of
data (CSV, XML, JSON, relational databases, etc.) into RDF using any 
RML compliant engine such as the 
[RMLMapper](https://github.com/RMLio/rmlmapper-java) 
or [RMLStreamer](https://github.com/RMLio/rmlstreamer). 
Afterwards, I presented the RMLStreamer and how you can use it to transform
huge amounts of data into RDF. The RMLStreamer leverages the 
Big Data platform [Apache Flink](https://flink.apache.org/) to scale the RDF
generation across CPUs (vertically), but also across machines (horizontally).

I was amazed by the response of the community when using all the tools
during the tutorial. I never expected that so many people would be 
interested in the work that I and the others of the knowledge graph construction
community are doing!

### Day 2: Tutorials & Workshops II: PhD symposium & KGC workshop

During the second day, I had to present my plan for my PhD (["Balancing RDF generation from heterogeneous data sources"](https://dylanvanassche.be/publications/#eswc2022-2)) at the
[PhD Symposium](https://2022.eswc-conferences.org/program-phd-symposium/)
track of ESWC. This track allows PhD students to get feedback from seniors
in the community related to their plans, approach, evaluation, etc.
I was the first presenter of the track, so I didn't really know what to expect.
The whole room was pretty filled with people queuing outside the room, but I
didn't really have stress. The Semantic Web community is constructive and nice
so I had nothing to fear! After presenting my work, 
I got some constructive feedback and comments
which I can definitely use in the next stage of my PhD. I would like to thank 
all the attendees for all their feedback to push my PhD to the next level.

As mentioned before, my schedule was really full which made it hard to attend other talks as I had to present in various places.
While, I had to attend the whole PhD Symposium,
I simply couldn't as it overlapped with the 
[Third International Workshop on Knowledge Graph Construction](https://kg-construct.github.io/workshop/)
where I had to present another short paper ["Continuous generation of versioned collections’ members with RML and LDES"](https://dylanvanassche.be/publications/#eswc2022-2). 
In this paper, we set the first steps towards incorporating data updates
duing knowledge graph construction with declarative mapping languages such as RML.
By leveraging these data updates, we could reduce the generation time
and materialized RDF triples significant.
However, these results are still preliminary,
we consider to execute a more detailed performance evaluation later on.

### Day 3: Main conference I: Posters!

Day 3 was the first main conference day, the day when I had to present my poster.
It was my last presentation of the conference, so I had to be awesome once more
in the evening. During the day various talks were given and an awesome minutes madness.
I presented my poster during the evening reception where I met so many awesome
people with constructive and good feedback. Thanks a lot of everyone for coming
by! It was so busy I didn't have a chance to grab something eat or drink,
luckily my supervisor brought me some food for which I'm very grateful.

### Day 4: Main conference II: Gala dinner 

The second day of the main conference, was kicked off by 
[Axel Ngonga](https://2022.eswc-conferences.org/keynote-speakers/#Axel)
with a keynote in which he explains how important representation is
when doing structured machine learning.
Later on, we had a session where papers were presented around dealing with
multiple sources. After lunch, I went to the Industry Track
to see what Siemens, Bosch, etc. were doing with Semantic Web technologies.
Siemens is using [W3C Web of Things (WoT)](https://www.w3.org/WoT/)
to describe complete buildings for automation purposes.
It was really nice to see these W3C WoT specifications being used in the industry.
After the coffee break I went to Linked Data Analysis track where a research
paper around [XSD binary floating point datatypes](https://2022.eswc-conferences.org/wp-content/uploads/2022/05/paper_98_Keil_et_al.pdf) was presented.
It really reminded me of a course I had a few years ago where floating point
errors were discussed. The same errors also occur with XSD floating point data
types used in RDF. It is worth to investigate if these errors also occur when
generating RDF from heterogeneous data sources such as with [RML](https://rml.io). 
Even better: how to prevent or reduce these errors during knowledge graph construction!
Not only the datatype may cause these errors,
but maybe also during the transformation from heterogeneous data sources to RDF.
Something worth to consider since they forget about it when launching the Ariane 5 rocket.
[A floating pointing error, caused the rocket to crash during a test flight](https://en.wikipedia.org/wiki/Ariane_5#Notable_launches).
In the evening, we had the gala dinner with all the people of the conference,
it was really nice and awesome to see everyone in a great dance mood!

### Day 5: Main conference III: All things come to an end

Before you know it, it is already the last day of the conference!
The opening keynote by [Tova Milo](https://2022.eswc-conferences.org/keynote-speakers/#Tova)
was amazing and really well explained.
After the keynote I went to the Software Development track,
I have an engineer background after all :).
One of the papers was about [lazy loading of RDF in applications](https://2022.eswc-conferences.org/wp-content/uploads/2022/05/paper_149_Kamburjan_et_al.pdf).
I thought of that as well in the past and
I can say that it really makes a difference, especially when dealing with a lot of RDF!
For the last session, I picked the Reasoning Systems one because I really
wanted to see [a reasoner on an Arduino](https://2022.eswc-conferences.org/wp-content/uploads/2022/05/paper_39_Bento_et_al.pdf).
They actually did it which it pretty awesome!
Most of the time we as Semantic Web researchers deal with enormous amount of data and systems.
This is the first time I saw such technologies being used in something like an Arduino
with limited RAM and CPU resources.

### Conclusion

ESWC 2022, my first in-person conference, was AWESOME! The bar is now pretty high for the next conferences :)
I met so many new people and really enjoyed the beautifull view of Crete.
I hope to see everyone soon again on the next conference or the next ESWC!

If you want to checkout my papers and posters published at ESWC 2022, you can find them on my [publication page](https://dylanvanassche.be/publications).
