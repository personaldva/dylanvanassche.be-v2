---
layout: post
title:  End of an era
date:   2020-08-25 12:00:00
author:
    id: https://dylanvanassche.be/#me
    name: Dylan Van Assche
description: Moving on from Jolla's Sailfish OS
---

I will list here why I'm moving away from Jolla's Sailfish OS, the mobile 
Linux OS I praised for more than 5 years!
In order to sketch the picture completely, some history:

Since FOSDEM 2019, [Pine64](https://pine64.org) announced their famous 
[PinePhone](https://pine64.org). 
The PinePhone is a real Mobile Linux device with Open Hardware and Open Source 
software.
The PinePhone has only a small amount of blobs to drive the WiFi/Bluetooth chip
and the modem RF stack.
Even the bare bone Linux distribution running on the modem has recently been 
made FOSS!
It's a real competitor for the [Librem5](https://puri.sm/products/librem-5/) 
from [Purism](https://puri.sm), except that the hardware is less powerfull and 
that's reflected in the price of both devices.
All the OSes that are available for the PinePhone are fully FOSS, except 
Sailfish OS.
Sailfish OS has a FOSS middleware from which Nemo Mobile profits, but the UI
and Jolla apps are closed source.

## The Good

I joined the Sailfish OS boat to get away from the Android/iOS closed gardens
and take back my privacy.
I wanted to be in control of my device, not Google or Apple.

### 1. Active community

The Sailfish OS community is actively working on 
[Mer](https://www.merproject.org) and [Sailfish OS](https://sailfishos.org).
Communication happens over IRC, Telegram or Matrix.
They are always happy to help when you're in trouble :smiley:

They were present at [FOSDEM](https://fosdem.org) where discussions happened in 
real life!

### 2. Running Android apps

Jolla integrated Aliendalvik into their phones. 
Aliendalvik allowed me to run the Android apps I cannot miss such as Waze for 
example.
Works really nice, the older phones aren't getting the latest version, but the
Xperia XA2 and Xperia 10 have Aliendalvik with Android 8.1!

### 3. App development

I learned a lot about Mobile App development over these years.
I developed a Tinder client (Sailfinder), an iRail client (BeRail) and 
maintained a Facebook webapp (Sailbook).
I even used Sailfish OS in my Master's thesis!

## The Bad

Besides having a really nice community, fantastic UI, FOSDEM meetings, etc. 
some things went wrong...

### 1. FOSS promises were never fulfilled

Back in 2013, when Jolla launched their Kickstarter for the first Jolla Phone,
they promised to make all components FOSS.
As of today (2020), the apps and UI are still closed source.

Such a situation doesn't look good in the FOSS world and won't help to attract
FOSS developers. 
Fortunally, [Nemo Mobile](https://nemomobile.net/) provides an fully FOSS 
alternative for the UI and apps.

### 2. Available software isn't up-to-date

Sailfish OS still uses Qt 5.6, an ancient Qt version which isn't supported 
anymore by the Qt Company.
The biggest problem is the license of newer Qt versions.
Jolla needs to pay for the Qt Company if it wants to use Qt in their closed 
source components since the newer Qt versions are licensed under GPLv3 instead
of GPLv2.
This makes it hard to port existing software.
Nemo Mobile doesn't have the license problem and provides the latest LTS 
version of Qt (5.12) in its repos.

The same argument holds for the Browser, another problem for Sailfish OS.
The current Browser engine is based on Gecko 38 and will be upgraded soon to 
Gecko 52. 
However, the upgraded engine is still from 2017 and light years behind the 
current state of the Web.

### 3. Priorities changed

Over the past years, Jolla had to shift their priorities to survive.
Jolla shifted to a B2B strategy and that seems to be succesful to survive.
However, the community is left out, the community they need for making 
Sailfish OS a success.

The latest change is the [closure of the public Mer bugtracker, the closure of
the Sailfish OS build service (OBS) and moving the Mer components to Github](https://forum.sailfishos.org/t/changes-needed-to-merge-the-project-names-to-sailfish-os/1672)
while it's not clear why these actions are needed to merge Mer into the 
Sailfish OS project.
This is one of the biggest problems Jolla had over all these years: 
communication with the community.

## The Ugly?

Not really :smiley: I had an amazing time on the Jolla boat and in the Sailfish
OS community, but the time has come to move on.

I totally understand why Jolla had to shift their priorities, because of that,
I need to shift mine to other Mobile Linux projects such as 
[postmarketOS](https://postmarketos.org) or 
[Mobian](https://mobian-project.org).

This means that from now on my Sailfish OS apps such as Sailbook, Sailfinder, 
BeRail are not maintained anymore.
I thank the Sailfish OS community and Jolla for these awesome years and I hope
we will cross paths again in the future!
