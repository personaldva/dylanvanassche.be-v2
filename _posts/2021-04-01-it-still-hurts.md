---
layout: post
title:  It still hurts
date:   2021-04-01 12:00:00
author:
    id: https://dylanvanassche.be/#me
    name: Dylan Van Assche
description: Why?
---

One year ago, I would be celebrating [April Fools' Day](https://en.wikipedia.org/wiki/April_Fools%27_Day), 
the only day in the year when you can prank without receiving any punishment for it :smile:

However, [last year, it wasn't a celebration](https://dylanvanassche.be/blog/2020/goodbye-my-friend/), 
it was the start of period of grief, pain and tears. 
01-04-2020 was the day when I lost my best friend, my sister I never had, my soulmate.

Ellen, 

Until this day, I'm still wondering Why? Why? Why? but as I know you, 
you had your reasons, strong reasons, 
and I will never judge you because of that.

I hope you found peace where ever you are and that you're happy.
That's the most important part for me, that you found what you were looking for.

Nevertheless, I still think about you every day because you were and are still
the most incredible person I have ever met :heart:.

XXX<br>
Your swimming mate,<br>
Dylan
