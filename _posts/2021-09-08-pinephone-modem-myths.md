---
layout: post
title:  PinePhone modem myths
date:   2021-09-08 12:00:00
author:
    id: https://dylanvanassche.be/#me
    name: Dylan Van Assche
description: Factchecking some PinePhone modem myths
---

I worked for a while now on 
[ModemManager](https://gitlab.freedesktop.org/mobile-broadband/modemmanager), 
[eg25-manager](https://gitlab.com/mobian1/devices/eg25-manager) 
and other parts of the modem stack for the PinePhone EG25-G modem.
During this, I gained some interesting insights about how the modem stack works,
where bugs may occur and how to solve them.
However, I have read over the past few weeks some misconceptions about some
parts of the modem stack which mislead users looking for a fix.
This situation hurts my work on the modem stack, 
hence this blog post to fact check some PinePhone modem myths.

### Myth 1: The PinePhone modem dissappears with quick resume support

Some distros seem to advise users to edit the service file of ModemManager
to change `--test-quick-suspend-resume` to `--test-no-suspend-resume`, which is
a bad idea in general.

One of my contributions to ModemManager for the PinePhone was the 
implementation of faster suspend/resume operations.
This contribution will be included in the upcoming 
[ModemManager 1.18 release](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/blob/master/NEWS)
to allow the PinePhone directly ring when a call is received in deep sleep.

Before these patches, we tested this approach with the `--test-no-suspend-resume`
which turns out to be fine for most of the use cases.
However, in some edge cases, the internal modem state may get out of sync
with ModemManager.
If ModemManager gets out of sync, the modem may crash internally.
This series of patches does the same as `--test-no-suspend-resume`, but also
synchronizes the internal modem state with ModemManager.

**FACT**: Always use `--test-quick-suspend-resume` in favor of `--test-no-suspend-resume`.
If you still experience issues, a debug log of ModemManager is welcome on the 
[ModemManager issue tracker](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/issues).

### Myth 2: The PinePhone wakes up but doesn't ring

Some distros blame the modem firmware for not receiving incoming calls when
they upgraded to ModemManager 1.18 RC1, which is simply not true.

1. ModemManager 1.18 RC1 is a release candidate for distros to test 
the new release and find any regressions. It is never meant to be included in
official images of distros. It can be available of course via the repos 
for users to install it and try it out, but it shouldn't be the default. 
If your distro does this, you have to report this as an issue to your distro.
2. ModemManager 1.18 brings QMI Voice support which means that all your calls
are handeled through QMI instead of AT commands and URCs. The PinePhone modem
has a bug in the firmware or USB stack which causes it to miss sometimes a
QMI indication to trigger an incoming call on resume.
Consequenctly, ModemManager won't notice the incoming call,
which means no ringtone at all.
I noticed this problem before some distros start to advise users to upgrade
their modem firmware (which is not the proper fix). 
I created a patch to overcome this problem which is reviewed and merged upstream: 
[MR 605](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/merge_requests/605).
The patch leverages AT URCs to reload the call information over QMI, 
so your phone should always ring, even if the QMI indication wasn't received.

**FACT**: Do not upgrade the firmware just to get this problem fixed, it won't.
Rather ask your distro to upgrade to ModemManager 1.18 when it is released or
backport my patches to their fork.

### Myth 3: Upgrading the firmware is a fix for everything

Recently, users have been encouraged to upgrade their 
[Quectel modem firmware](https://github.com/Biktorgj/quectel_eg25_recovery/) 
or try 
[alternative FOSS firmware](https://github.com/Biktorgj/pinephone_modem_sdk/) 
as a fix for bugs some distros are facing.
This is again simply not true, it is way more complicated than that.

PinePhone modems ship with the oldest publicly known version of the firmware.
This firmware generally works and has a limited number of VoLTE profiles available.
Several firmware upgrades exist for the modem, some more stable than the other,
which may make the modem more stable, or not.
One thing for sure, each upgrade makes more VoLTE profiles available, so if you
need VoLTE, upgrading might be a good idea.
If you have an unstable modem, upgrading to `EG25GGBR07A08M2G_01.002.01.002` may
also be a good idea, but it may not solve all your issues.
A lot also depends on your carrier and network you're using your PinePhone on.
Calling your carrier's support may help, sometimes they can improve things on 
their end as well, but don't expect magic of course :smiley:
My situation improved with the `EG25GGBR07A08M2G_01.002.01.002` 
but your mileage may vary.

Upgrading the firmware may help your use case, but it is not a fix
for all the modem issues. On the one hand, the modem stack is very complex, 
while on the other hand, users expect it to be flawless because it is a phone!
The modem stack on the PinePhone mostly consists of:

- Quectel EG25-G modem with Quectel firmware or alternative FOSS firmware
- USB stack and `qmi_wwan` driver of the Linux kernel
- ModemManager with `libqmi` or oFono
- Dialer and text applications such as GNOME Calls, Chatty, Plasma Dialer, etc.
- Available networks, providers and SIM cards
- `callaudiod`, ALSA UCM, PulseAudio/PipeWire for routing call audio during calls
- `mmsd` and `mmsd-tng` for MMS
- `vvmd` for Visual Voicemail

All these pieces need to work flawlessly together which is not an easy task.
Distros should properly test their whole modem stack and locating 
where bugs occurs so they can be fixed upstream.
If the bug occurs in the modem firmware, an upgrade can be considered, 
but it may also occur anywhere else in the stack.

Another important warning regarding upgrading the modem firmware is that 
it also contains a risk of bricking the modem.
If that's the case, the user must remove the back cover and shorten two pins 
to enter EDL mode (and hope it works). 
That's something you definitely don't want to do when you
rely on you daily drive your PinePhone.

**FACT**: Before upgrading the firmware, it's better to first locate where the
bugs comes from, it may come from somewhere else in the modem stack than 
the modem firmware itself.
Therefore, upgrading the firmware isn't a fix for everything, a lot more things
come into play.
However, if you need more VoLTE profiles or want to experiment, go ahead!

