---
layout: post
title:  Switching to Alpine Linux
date:   2021-01-31 12:00:00
author:
    id: https://dylanvanassche.be/#me
    name: Dylan Van Assche
description: Small. Simple. Secure.
---

### Goodbye Arch Linux, hello Alpine Linux!

Over the last couple of years,
I was a heavily [Arch Linux](https://archlinux.org) user.
Arch Linux is fast, has a great community 
and provides a lot of software through [AUR](https://aur.archlinux.org).
However, it has some disadvantages as well:

- Steep learning curve for non-Linux users as you need 
to bootstrap your system yourself from scratch. 
- AUR can contain any software, you have to trust the maintainer 
that the software is sane, gets regulary updates 
and does not break your system. 
Some packages are compiled from git on your system, 
this can make the installation of updates heavy, long and annoying.
- It's based on [systemd](https://systemd.io) 
which is [controversial](https://suckless.org/sucks/systemd/) 
in the Linux world 
and I dealt in the past as well with `systemd`'s annoying 'features'.

So, while switching from Jolla's [Sailfish OS](https://sailfishos.org) 
on my mobile devices to 
[postmarketOS](https://postmarketos.org) (Alpine Linux based), 
I was hooked by the way how Alpine Linux works in terms of packaging, 
system operation and does not use `systemd`!

### Installing Alpine Linux

Alpine Linux is fairly easy to install 
if you don't need any special partitioning 
or such with the `setup-alpine` script.

1. Get the latest Alpine Linux image from 
[their Downloads' page](https://alpinelinux.org/downloads).
2. Burn it on an USB drive and boot your device from it.
3. A default shell appears, login as `root` with no password.
4. Run `setup-alpine` and follow the instructions.

I picked a traditional system install ('Sys Disk Mode') 
as I wanted to install Alpine Linux on my desktop.
A complete guide on installing Alpine Linux 
can be found on their well documented wiki: 
https://wiki.alpinelinux.org/wiki/Installation
