---
layout: post
title:  Words are not enough
date:   2020-09-15 09:00:00
author:
    id: https://dylanvanassche.be/#me
    name: Dylan Van Assche
description: I still think about you every day
---

It's already more than 5 months since you passed 
away [Ellen](https://dylanvanassche.be/blog/2020/goodbye-my-friend/).
You filled a special place in my heart, a place that never could be filled by 
anyone else.
You were and still are my best friend, I think about you every day.

<div class="col three caption">
    <img class="col three" src="{{ site.baseurl }}/assets/img/best-friend-2.jpg">
</div>

Because of the whole Covid-19 crisis, we had to say goodbye to you without much
room for grieving.
Now that will change soon. During the ceremony, I will say a few words about you
 and how I knew you. 
However, words are not enough to express what we had.

### Words are not enough

<b>Apologies, but the text below is only available in Dutch.</b>

*Meer dan 5 jaar geleden, tijdens oudejaarsavond, was ik door mijn contacten aan
het scrollen om ze allemaal een SMSje te sturen met mijn nieuwjaarswensen. 
Ook jij, Ellen, kreeg zo'n SMSje. Ik had je nummer nog van toen we samen in Het
Atheneum van Vilvoorde zaten.
Doordat je verandert was van school, hadden we het contact wat verloren.
Maar dat SMSje veranderde veel, heel veel.*

*Sinds dat SMSje begonnen we terug met elkaar te praten over letterlijk alles. 
Het was precies of ik je al jaren kende.
Aangezien we beide waterratten zijn, spraken we wel eens af om te gaan zwemmen.
Elke keer we gingen zwemmen, keek ik er zo naar uit om weer te kunnen vertellen 
over wat er allemaal was gebeurd in ons leven.*

*Over al die jaren bleef onze band verder groeien.
Dat kon alleen maar doordat we elkaar begrepen en steunde als 1 van ons het 
super hard nodig had.
Ik kon op elk moment van de dag bij jou terecht als ik met iets zat en 
omgekeerd.
Soms vervaagde het contact een beetje, maar het was nooit weg dankzij de 
fantastische band die we hadden.
Van zodra we elkaar terug zagen, pikten we de draad opnieuw op alsof we elkaar 
gisteren nog spraken. Een fantastisch gevoel om zo iemand in je leven te 
hebben.*

*Ik denk nog elke dag aan jou, Ellen en dat zal nooit veranderen.
Je was en zal altijd mijn beste vriendin blijven en niet vergeten, mijn 
zwemmaatje.
Je zit in een speciale plek in mijn hart :heart:. 
Een plek die niemand anders zou kunnen vullen.*

*Rust zacht, lieve Ellen.
<br>XXX Je zwemmaatje*

