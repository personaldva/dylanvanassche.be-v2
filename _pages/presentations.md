---
layout: page
permalink: /presentations/
title: Presentations
description: My presentations, chronological ordered.
---

<h3 class="year">2022</h3>

- [KG4DI Kick-off event: Knowledge Graph Construction Tutorial]({{ site.baseurl }}/assets/pdf/kg4di-tutorial-slides.pdf)
- [PrivCon2022: Degoogling your life]({{ site.baseurl }}/assets/degoogling-your-life)
- [ESWC2022: KGC Tutorial 2022]({{ site.baseurl }}/assets/pdf/eswc2022-kgc-tutorial-slides.pdf)
- [ESWC2022: Continuous generation of versioned collections’ members with RML and LDES]({{ site.baseurl }}/assets/pdf/eswc2022-rml-ldes-slides.pdf)
- [ESWC2022: Balancing RDF generation from heterogeneous data sources]({{ site.baseurl }}/assets/pdf/eswc2022-phd-symposium-slides.pdf)
<h3 class="year">2021</h3>

- [ICWE2021: Leveraging Web of Things W3C recommendations for knowledge graphs generation]({{ site.basurl }}/assets/leveraging-wot-for-kg-generation)
