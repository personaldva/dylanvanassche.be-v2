# README

1. Enter search keyword
2. Set to 'title' and 'abstract'
3. Use Jan 1999 or later
4. Modify query from AND to OR between title & abstract
5. Press search
6. Get the first 200 results by changing the GET parameters
