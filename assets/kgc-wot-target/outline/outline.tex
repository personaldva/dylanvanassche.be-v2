% ICWE2021: Leveraging W3C Web of Things for knowledge graphs generation
% (c) Dylan Van Assche (2021) IDLab - Ghent University - imec
%--------------------
% Packages
%--------------------
\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{calc} 
\usepackage{enumitem}
\usepackage[all]{nowidow}

%-----------------------
% Begin document
%-----------------------
\title{ICWE2021: Leveraging W3C Web of Things for knowledge graphs generation}
\author{Dylan Van Assche}
\begin{document}
\maketitle
\section{Outline}
\subsection{Attention getter}
If you have ever created a workflow to generate a knowledge graph,
you may have used mapping languages to declaratively describe 
the generation process.
But can you completely describe your workflow in these mapping languages? 
Without any additional pre- or post-processing? 

Well, let's go back first to where it all begun:
\begin{enumerate}
    \item \textbf{Hardcoded scripts}: In the early days of the Semantic Web, 
        knowledge graphs were generated through hardcoded scripts, 
        each script was tailored towards the specific use case.
        For every change, the script must be revised and the knowledge graph 
        could be generated again. However, this approach wasn't really 
        sustainable. Therefore, a replacement was needed 
        for these hardcoded scripts.
    \item \textbf{Mapping Languages}: Mapping Languages were introduced 
        together with a processor to overcome this problem. 
        Instead of hardcoding the generation in a script, a generic processor 
        reads mapping rules which described the generation of a knowledge 
        graph. For each use case, a new set of mapping rules was written while 
        the processor could be re-used. However, mapping languages assumed that
        the data was already available in a specific and completely cleaned. 
        An additional data transformation script was added, but this made the 
        workflow again unsustainable as each use case required a specific data
        transformation script. Therefore, another solution for transforming
        the data was necessary.
    \item \textbf{Transformation rules}: To address this problem,
        additional transformation rules were integrated with existing 
        mapping languages. The processor reads these transformation rules and 
        can apply the transformation directly during the knowledge graph 
        generation. 
\end{enumerate}

\subsection{Need}
However, some additional pre- and post-processing is still needed today! 
Mapping Languages do not consider data sources such as Web APIs or streams, 
nor do they consider how the generated knowledge graphs are exported to one or 
multiple targets! This reduces again the reproducibility of your workflow, 
as each specific use case requires different pre- and post-processing scripts.

\subsection{Task}
I'm Dylan Van Assche from IDLab, Ghent University 
and I will present our paper 
``\textit{W3C Leveraging Web of Things for Knowledge Graphs generation}''.
in which we propose a solution to integrate 
these pre- and post-processing steps into existing processors.
With our contributions, mapping language processors can access Web APIs 
and streams directly using W3C recommendations, 
and export their generated knowledge graphs to multiple targets 
such as SPARQL endpoints or RDF dumps.

\subsection{Main message}
We propose to align  Web of Things W3C recommendations with mapping languages 
to access Web APIs and streams and 
introduce Logical Target to specify how a knowledge graph should be exported.
W3C Web of Things recommendations and Logical Target 
make your knowledge graph generation pipeline 
more reproducible across environments and machines.

\subsection{Preview}
\begin{enumerate}
    \item What is a mapping language and RML?
    \item What is W3C Web of Things and alignment with mapping languages?
    \item What is Logical Target and how does it fit within mapping languages?
    \item Why using Web of Things and Logical Target in a KG generation 
          pipeline?  
\end{enumerate}

\subsection{Body}
\paragraph {First things first, what is a mapping language?}
Mapping languages are declarative descriptions for generating knowledge graphs.
These languages describe how data should be transformed into knowledge graphs.
A possible serialization of knowledge graphs is the W3C recommendation 
of the Resource Description Framework or in short, RDF.
These knowledge graphs allow to combine knowledge from various data sources 
which can be queried by applications for various purposes.
Mapping languages are also re-usable and independent 
as any mapping language processor can use the same mapping rules 
to generate a knowledge graph.
Examples of mapping languages are the W3C recommendation R2RML 
for generating RDF from relational databases and the broader
RDF Mapping Language (RML) for generating RDF from heterogeneous data.

RDF Mapping Language or in short RML, is an extension of R2RML.
It broadens the scope of R2RML from relational databases 
to any heterogeneous data source by leveraging external data access 
descriptions such as D2RQ, DCAT or CSVW.
RML describes how RML Processors should generate RDF 
from various heterogeneous data sources when executing RML mapping rules.

An RML Processor fetches data from heterogeneous data sources 
such as relational databases, XML, JSON or CSV files, 
applies the RML mapping rules to generate a knowledge graph in RDF.

However, RML currently doesn't support Web APIs or streams, 
nor does any mapping language describes how a knowledge graph 
should be exported to one or multiple Targets.

\paragraph{Now that we know what mapping languages are and RML is, 
what is W3C Web of Things and how are they aligned with mapping languages?}
W3C Web of Things are a set of W3C recommendations 
released by the Web of Things Working Group to describe Internet of Things 
devices, streams and Web APIs in a declaratively way.
They not only consider devices, but also the architecture 
and more importantly, the security of such devices.

We reused these recommendations to declaratively describe 
how Web APIs and streams are accessed in mapping languages.
Furthermore, these recommendations are also used to describe 
the security requirements of Web APIs or streams 
since these resources are commonly protected by authentication.

In the example, we access the Web API of our use case.
\begin{itemize}
    \item \texttt{td:PropertyAffordance} describes where our Web API resource 
        is located and how a mapping language processor has to access it. 
        Protocol-specific features can be supplied as well. 
        In this example, a HTTP Web API is accessed, but other Web APIs and 
        streams such as MQTT or CoAP can be accessed as well!
    \item \texttt{wotsec:APISecurityScheme} describes the security 
        requirements of our Web API. In this example, the Web API is 
        protected with an API key which must be supplied in the headers 
        of the request.
    \item \texttt{td:Thing} links the security description with the 
        access description of the Web API.
\end{itemize}

We validated our approach in 2 use cases: ESSENCE and DAIQUIRI 
with 2 different RML processors: the RMLMapper and RMLStreamer.
In ESSENCE, we accessed a Web API which required authentication using a token.
Therefore, we described the Web API endpoint and authentication 
using WoT in the mapping rules. The RMLMapper could directly access the 
Web API and generate the knowledge graph.
In the DAIQUIRI use case, we access a MQTT stream through the WoT descriptions.
The RMLStreamer leverages these descriptions to directly access the MQTT stream 
and generate the knowledge graph.

We can now access Web APIs and streams as a data source, 
but we still can't describe the output of our knowledge graph.

\paragraph{That's my third point: What is Logical Target?}
Logical Target is a declarative description how a generated knowledge graph 
must be exported. We leveraged existing data access descriptions 
to describe how the target must be accessed.
This lower the implementation cost as you can re-use the same parts 
you implemented already for retrieving data from various sources.
Besides access, Logical Target also describes in which format 
a knowledge graph must be exported and if compression must be applied or not.
Moreover, multiple targets can be specified for the same knowledge graph.
This way, you can make directly local backups of your knowledge graph 
while exporting it to your production setup.

In the example, we export our generated knowledge graph from our use case
to two different targets: a SPARQL endpoint using SPARQL UPDATE queries 
and to a local file on disk.
\begin{itemize}
    \item \texttt{rmlt:target} specifies the access description for the target.
        We leveraged here external vocabularies such as W3C VoID 
        or W3C SPARQL Service Description to not limit our approach to a fixed
        list of targets.
    \item \texttt{rmlt:serialization} describes the serialization format, 
        in this use case, we export the knowledge graph as Turtle 
        to a local file to create backups.
    \item \texttt{rmlt:compression} to save space for our backups, we compress 
        them with GZip compression.
\end{itemize}

We validated Logical Target as well in both use cases: ESSENCE and DAIQUIRI.
In both use cases, we export our knowledge graph to 2 different targets: 
a local file with N-Quads and GZip compression and 
a SPARQL endpoint with SPARQL UPDATE queries.
Thanks to our approach, the knowledge graph is only generated once and exported 
to all targets at once.

\paragraph{Alright, but what are the benefits of using 
Web of Things and Logical Target in a KG generation pipeline?}
\begin{itemize}
    \item \textbf{Web APIs and streams}:
        Web APIs and streams require additional tooling 
        to perform authentication and retrieve their data 
        before the generation can happen.
        Our alignment with Web of Thing W3C recommendations allows
        to retrieve data directly from Web APIs or streams, 
        even when they require authentication.
    \item \textbf{Export in a generic way to multiple targets}
        Existing tools only export your knowledge graph to a single target and
        don't have a generic description to define this process.
        Logical Target can be used to export a single knowledge graph to 
        multiple targets in various formats. Moreover, you can export also 
        parts of your knowledge graphs to specific targets.
        You have complete control over how your knowledge graph is exported!
    \item \textbf{Reproducibility of the pipeline}: 
        Your knowledge graph generation pipeline works in any environment 
        or on any machine as your pipeline is not only described on the input, 
        but also on the output without using any additional hard-coded scripts.
\end{itemize}

\subsection{Review}
\begin{enumerate}
    \item W3C Web of Things describes access to Web API and streams 
        for mapping languages,
    \item Logical Target describes how a generated knowledge graph 
        must be exported,
    \item to improve the reproducibility of existing 
        knowledge graph generation pipelines.
\end{enumerate}

\subsection{Conclusion}
In conclusion, If multiple processors implement this approach, 
you can change the processor in your generation pipeline 
without modifying any hard-coded
scripts or parameters as your pipeline is fully declaratively described 
in the mapping rules.

\subsection{Close}
So, if you are into knowledge graph generation,
consider giving our approach a try!
Our approach is implemented in two RML mapping language processors: 
RMLMapper and RMLStreamer.

\end{document}
